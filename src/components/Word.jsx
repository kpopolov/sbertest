import React from 'react';
import Letter from './Letter';

const Word = ({word}) => {
    const letters = word.split('');
    return (
      <div className="Word">{letters.map((l, key) => <Letter key={key} text={l}/>)}</div>
    );
};

export default Word;
