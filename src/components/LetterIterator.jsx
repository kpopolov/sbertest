import React, {PureComponent} from 'react';
import Letter from "./Letter";

class LetterIterator extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            currentLetters: [' ', ' ']
        };

        this.iter = this.props.word[Symbol.iterator]();
    }

    handleLetterClick(index) {
        const nextLetters = this.state.currentLetters.slice();
        const next = this.iter.next();
        if (next.done) return;
        nextLetters[index] = next.value;
        this.setState({currentLetters: nextLetters});
    }



    render () {
        return (
            <div>
                {this.state.currentLetters.map((l, i) => <Letter key={i} text={l} onClick={() => this.handleLetterClick(i)}/>)}
            </div>
        );
    }
}

export default LetterIterator;
