import React from 'react';
import './Letter.css';

/**
 * Компонент для отображения букв
 * @param {string} [text='']
 * @param {function} [onClick]
 * @return {JSX}
 */
const Letter = ({ text = '', onClick}) => {
    return (<div style={onClick ? {} : {cursor: 'pointer'}} onClick={onClick} className="Letter">{text}</div>);
};

export default Letter;
