class Str extends String {
    constructor(string) {
        super(string);
    }

    reverse() {
        return new Str(this.split('').reverse().join(''));
    }

    toUpperCase() {
        return new Str(String(this).toUpperCase());
    }
}
