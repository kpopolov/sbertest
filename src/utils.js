import qs from "qs";

export const getWordFromSearch = () => {
    const {word = ''} = qs.parse(window.location.search.replace(/^\?/, ''));
    return word;
};
