import React, { PureComponent } from 'react';
import './App.css';
import Word from "./components/Word";
import LetterIterator from "./components/LetterIterator";
import { getWordFromSearch } from "./utils";

class App extends PureComponent {
    render() {
        const word = getWordFromSearch();
        return (
            <div className="App">
                <h1>Тестовое задание</h1>
                <h2>Второй пункт — отображение слова из get-параметра</h2>
                <Word word={word}/>
                <h2>Третий пункт — итератор по буквам</h2>
                <LetterIterator word={word}/>
                <h2>Четвертый пункт см в файле StringBonus.js</h2>
            </div>
        );
    }
}

export default App;
